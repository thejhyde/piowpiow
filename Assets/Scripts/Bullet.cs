using UnityEngine;
using UnityEngine.Assertions;

namespace gc {

	public class Bullet : MonoBehaviour {

		public float Speed;
		public float Damage;

		private static UnityEngine.Object bulletPrototype;

		internal static void Fire(Vector2 fireDirection, Vector3 position){
			if (bulletPrototype == null) {
				bulletPrototype = ResourceLoader.Instance.GetResource("Prefabs/Bullet");
			}

			GameObject bulletObject = Instantiate(bulletPrototype, position, Quaternion.identity) as GameObject;
			Bullet bullet = bulletObject.GetComponent<Bullet>();
			Rigidbody2D body = bulletObject.GetComponent<Rigidbody2D>();
			body.AddForce(fireDirection * bullet.Speed, ForceMode2D.Impulse);
        }

		void OnTriggerEnter2D(Collider2D coll){
			if (coll.gameObject.CompareTag("ENEMY")) {
				Enemy enemy = coll.gameObject.GetComponent<Enemy>();
				Assert.IsNotNull(enemy, "Object tagged Enemy but no Enemy script attached");
				enemy.SendMessage("ShootReaction"); 
			}
			Destroy(gameObject);
		}
	}

}

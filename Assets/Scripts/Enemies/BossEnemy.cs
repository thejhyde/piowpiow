﻿using UnityEngine;
using System.Collections;

namespace gc{
	public class BossEnemy : Enemy {

		delegate void MoveAction();
		MoveAction ma = ()=> {};

		protected override void Awake(){
			base.Awake();

			WaitTask wait = new WaitTask(1000);
			Services.taskManager.AddTask(wait);
			wait.Then(new ScaleUpTask(transform, 4));
			wait.Then(new ActionTask(()=> { ma = fireBullets;}));
			wait.Then(new WaitForTask(isFifty));
			wait.Then(new ActionTask(()=> { body.velocity = new Vector2(0, 0); ma = chase;} ) );
		}

		public override void move(){
			base.move();
			ma();
		}

		private void chase(){
			Player player = Player.Instance;
			GameObject playerObject = player.gameObject;

			Vector2 directionToPlayer = DirectionTo(playerObject.transform.position);
			SteeringForce(directionToPlayer);
			SetAngle();
		}

		private void fireBullets(){
			Vector2 randomDirection = Random.insideUnitCircle;
			randomDirection = randomDirection.normalized;
			//Vector2 randomDirection = Vector2.left;
			Vector3 fireLocation = gameObject.transform.position + ((Vector3)randomDirection * 12);
			Bullet.Fire(randomDirection, fireLocation);
		}

		private bool isFifty(){
			return Health <= MaxHealth/2;
		}

		bool isThirty(){
			return Health <= (MaxHealth*3)/10;
		}
	}
}

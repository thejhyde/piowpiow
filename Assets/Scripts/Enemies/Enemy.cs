﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;

namespace gc {

	//This is my subclass sandbox!
	public abstract class Enemy : MonoBehaviour {
		public AudioClip wake;
		public AudioClip die;
		public int MaxHealth;
		public int Damage;
		public float MaxSpeed;
		protected Rigidbody2D body;
		public bool destroy = false;

		protected int Health;

		delegate void Reaction();
		Reaction shootReaction;

		protected virtual void Awake() {
			//AudioSource.PlayClipAtPoint(wake, Camera.main.transform.position);
			Health = MaxHealth;

			body = GetComponent<Rigidbody2D>();
            if (body == null) {
                throw new MissingComponentException("MovementController added to game object that has no Rigidbody2D");
            }
		}

		public void SetShootReaction(bool getsShot){
			if(getsShot){
				shootReaction = ApplyDamage;
			}else{
				shootReaction = IncreaseScale;
			}
		}

		public virtual void move(){

		}

		void OnCollisionEnter2D(Collision2D coll) {
			CollisionHandler(coll);
		}

		protected virtual void CollisionHandler(Collision2D coll){
			if (coll.gameObject.CompareTag("PLAYER")) {
				Player player = coll.gameObject.GetComponent<Player>();
				Assert.IsNotNull(player, "Object tagged PLAYER but no Player script attached");
				player.SendMessage("ApplyDamage", Damage); 
			}
		}

		void ShootReaction(){
			shootReaction();
		}

		private void ApplyDamage() {
			Health -= 100;
			if (Health <= 0) {
				destroy = true;
			}
		}

		private void IncreaseScale(){
			transform.localScale += new Vector3(0.1f, 0.1f, 0);
			MaxSpeed += 5;
		}

		void OnDestroy() {

		}

		//Gets the direct to a given object
		protected Vector2 DirectionTo(Vector2 goal){
			Vector2 position = gameObject.transform.position;

			Vector2 offset = goal - position;
			return offset.normalized;
		}

		//Determine the steering force (this is using Craig Reynolds' classic approach.
		//You can find out more here: http://www.red3d.com/cwr/steer/gdc99/)
		protected void SteeringForce(Vector2 direction){
			Vector2 desiredVelocity = direction * MaxSpeed;
			Vector2 steeringForce = desiredVelocity - body.velocity;
			Vector2.ClampMagnitude(steeringForce, MaxSpeed);
            body.AddForce(steeringForce);
			float currentSpeed = body.velocity.magnitude;
			if (currentSpeed > MaxSpeed) {
                body.velocity = Vector2.ClampMagnitude(body.velocity, MaxSpeed);
			}
		}

		// make the body face in the direction of movement
		//I feel this method could be generalized to be something less specific
		protected void SetAngle(){
			float angle = Mathf.Rad2Deg * (Mathf.Atan2(body.velocity.y, body.velocity.x)) + 180; // The extra 180° are because the enemy assets are pointing to the left
			body.MoveRotation(angle);
		}
	}
}


﻿using UnityEngine;
using System;
using System.Collections;

namespace gc {

	public class EnemyManager {

		ArrayList allEnemies;
		GameObject[] possibleEnemies;

		//All the data about waves
		int[] waves = {2, 1, 2, 3}; //which enemy we're spawning
		int[] waveAmount = {5, 2, 3, 1}; //How many enemies are in each wave
		float waveRate = 0.1f; //the rate at which they spawn
		int waveLength = 5; //How long they'll hang around

		int waveNumber = 0; //What wave are on?
		bool waveOver = false; //Is the wave over? Of course not, the game hasn't started yet

		bool spawning;
		MainScene scene;


		public EnemyManager(){
			possibleEnemies = Services.possibleEnemies;
			allEnemies = new ArrayList();
		}

		public void Start(){
			possibleEnemies = Services.possibleEnemies;
			allEnemies = new ArrayList();
			scene = (MainScene)Services.sceneManager.CurrentScene;
			StartWave(waveNumber);

		}

		public void Stop(){
			for(int i = allEnemies.Count - 1; i >= 0; i--){
				GameObject g = (GameObject)allEnemies[i];
				allEnemies.Remove(g);
				UnityEngine.Object.Destroy(g);
			}
		}
		
		public void Update () {
			//Goes through all the enemies
			for(int i = allEnemies.Count - 1; i >= 0; i--){
				GameObject g = (GameObject)allEnemies[i];
				//If there's a player, move all the enemies
				g.GetComponent<Enemy>().move();
				//Checks if they need to be destroyed or if the wave is over
				if( g.GetComponent<Enemy>().destroy || waveOver){
					allEnemies.Remove(g);
					UnityEngine.Object.Destroy(g);
					Services.eventManager.Fire(new EnemyDeathEvent());
				}
			}

			// If there are no enemies and all the enemies for this wave spawned already
			// spawn some new enemies.
			if ((allEnemies.Count == 0 || waveOver) && !spawning) {
				spawning = true;
				Services.taskManager.AbortAllTasks();
				StartWave(waveNumber);
				//increase wave amount and wave number
				waveNumber = (waveNumber + 1) % waves.Length;
				waveOver = false;
			}
		}

		private void StartWave(int index){
			WaitTask lastTask = new WaitTask(waveRate * 1000);
			Services.taskManager.AddTask(lastTask);
			for(int i = 0; i < waveAmount[index]; i++){
				SpawnEnemyTask s = new SpawnEnemyTask(wavePosition(i+1, waveAmount[index]+1), possibleEnemies[waves[index]], scene.health, scene.getsShot);
				lastTask.Then(s);
				WaitTask w = new WaitTask(waveRate * 1000);
				s.Then(w);
				lastTask = w;
			}
			lastTask.Then(new ActionTask(()=> { spawning = false;}));
			if(scene.timedWaves){
				WaitTask endWait = new WaitTask(waveLength * 1000);
				endWait.Then(new ActionTask(()=> { waveOver = true; }));
				Services.taskManager.AddTask(endWait);
			}
		}

		public void AddEnemy(GameObject enemy){
			allEnemies.Add(enemy);
		}

		//Lines them up on the right side of the screen
		private Vector3 wavePosition(float index, float total){
			Vector3 start = new Vector3(Config.WorldWidth-4, 1, 0);
			Vector3 end = new Vector3(Config.WorldWidth-4, Config.WorldHeight-1, 0);
			return Vector3.Lerp(start, end, (index/total));
		}
	}
}
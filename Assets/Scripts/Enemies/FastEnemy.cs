﻿using UnityEngine;

namespace gc {

	public class FastEnemy : Enemy {

		public override void move(){
			base.move();

			Player player = Player.Instance;
			GameObject playerObject = player.gameObject;

			Vector2 directionToPlayer = DirectionTo(playerObject.transform.position);
			SteeringForce(directionToPlayer);
			SetAngle();
		}
	}
}

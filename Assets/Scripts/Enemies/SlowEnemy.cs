﻿using UnityEngine;

namespace gc {

	public class SlowEnemy : Enemy {

		bool direction = false;

		protected override void Awake(){
			base.Awake();
			
		}

		public override void move(){
			base.move();
			if(direction){
				SteeringForce(Vector2.left*5);
			}else{
				SteeringForce(Vector2.right*5);
			}
		}

		protected override void CollisionHandler(Collision2D coll) {
			base.CollisionHandler(coll);
			direction = !direction;
		}
	}
}

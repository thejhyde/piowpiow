using UnityEngine;

namespace gc {

	public class StateEnemy : Enemy {

		private	 FSM<StateEnemy> _states;
		private bool direction;

		protected override void Awake(){
			base.Awake();

			_states = new FSM<StateEnemy>(this);
			_states.TransitionTo<MoveLeft>();
			direction = true;
		}

		public override void move(){
			base.move();
			_states.CurrentState.Update();
		}

		protected override void CollisionHandler(Collision2D coll) {
			base.CollisionHandler(coll);
			direction = !direction;
		}

		private class MoveLeft : FSM<StateEnemy>.State{
			private float time;

			public override void OnEnter(FSM<StateEnemy>.State previousState){
				time = 3;
				Context.direction = (Random.value < 0.5f);
			}

			public override void Update(){
				if(Context.direction){
					Context.SteeringForce(Vector2.left*5);
				}else{
					Context.SteeringForce(Vector2.right*5);
				}
				Context.SetAngle();
				time -= Time.deltaTime;
				if(time <= 0){
					TransitionTo<MoveDown>();
				}
			}
		}

		private class MoveDown : FSM<StateEnemy>.State{
			private float time;
			private GameObject player;
			
			public override void OnEnter(FSM<StateEnemy>.State previousState){
				time = 3;
				player = Player.Instance.gameObject;
				Context.direction = (Random.value < 0.5f);
			}

			public override void Update(){
				if(Context.direction){
					Context.SteeringForce(Vector2.up*5);
				}else{
					Context.SteeringForce(Vector2.down*5);
				}
				Context.SetAngle();
				time -= Time.deltaTime;
				if(time <= 0){
					TransitionTo<MoveLeft>();
				}

				if(Vector3.Distance(Context.transform.localPosition, player.transform.localPosition) <= 20){
					TransitionTo<Follow>();
				}
			}

			public override void OnExit(FSM<StateEnemy>.State nextState){
				if(nextState.GetType() == typeof(Follow)){
					((Follow)nextState).player = player;
				}
			}
		}

		private class Follow : FSM<StateEnemy>.State{
			public GameObject player;

			public override void Update(){
				Vector2 directionToPlayer = Context.DirectionTo(player.transform.position);
				Context.SteeringForce(directionToPlayer);
				Context.SetAngle();
				if(Vector3.Distance(Context.transform.localPosition, player.transform.localPosition) >= 30){
					TransitionTo<MoveDown>();
				}
			}
		}
	}
}

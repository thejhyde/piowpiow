﻿using UnityEngine;

namespace gc {

	public class StraightEnemy : Enemy {

		Vector2 currentDirection;

		protected override void Awake(){
			base.Awake();
			InvokeRepeating("FindPlayer", 0, 1);
		}

		public override void move(){
			base.move();

			SteeringForce(currentDirection);
			SetAngle();
		}

		private void FindPlayer(){
			Player player = Player.Instance;
			GameObject playerObject = player.gameObject;

			currentDirection = DirectionTo(playerObject.transform.position);
		}
	}
}
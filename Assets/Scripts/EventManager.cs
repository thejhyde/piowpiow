﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

namespace gc {

	public class EventManager{
		private Dictionary<Type, Event.Handler> registeredHandlers = new Dictionary<Type, Event.Handler>();

		public void Register<T>(Event.Handler h) where T : Event{
			Type type = typeof(T);
			if(registeredHandlers.ContainsKey(type)){
				registeredHandlers[type] += h;
			}else{
				registeredHandlers[type] = h;
			}
		}

		public void Unregister<T>(Event.Handler h) where T : Event{
			Type type = typeof(T);
			Event.Handler handlers;
			if(registeredHandlers.TryGetValue(type, out handlers)){
				handlers -= h;
				if(handlers == null){
					registeredHandlers.Remove(type);
				}else{
					registeredHandlers[type] = handlers;
				}
			}
		}

		public void UnregisterAll(){
			registeredHandlers.Clear();
		}

		public void Fire(Event e){
			Type type = e.GetType();
			Event.Handler handlers;
			if(registeredHandlers.TryGetValue(type, out handlers)){
				handlers(e);
			}
		}
	}
}

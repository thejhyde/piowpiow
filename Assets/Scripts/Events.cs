namespace gc {
	//This is the superclass for all events
	public abstract class Event{
		public delegate void Handler(Event e);
	}

	//What it sounds like
	public class EnemyDeathEvent : Event{
	}

	//What it sounds like
	public class PlayerDeathEvent : Event{
		public readonly float x;
		public readonly float y;

		public PlayerDeathEvent(float x, float y){
			this.x = x;
			this.y = y;
		}
	}

	//The player is done spawning
	public class PlayerSpawnedEvent : Event{
	}
}
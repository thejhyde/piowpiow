using System;
using System.Collections;
using System.Collections.Generic;

namespace gc{

	public class FSM<TContext>{

		private readonly TContext _context;

		private readonly Dictionary<Type, State> _stateCache = new Dictionary<Type, State>();

		public State CurrentState {get; private set; }

		public FSM(TContext context){
            _context = context;
        }

		public void Update(){
			if(CurrentState != null){
				CurrentState.Update();
			}
		}

		public void Clear(){
			foreach (var state in _stateCache.Values){
				state.CleanUp();
			}
			_stateCache.Clear();
		}

		public void TransitionTo<TState>() where TState : State{
			TState nextState = GetOrCreateState<TState>();
			State previousState = CurrentState;
			if(previousState != null){
				previousState.OnExit(nextState);
			}
			CurrentState = nextState;
			nextState.OnEnter(previousState);
		}

		private TState GetOrCreateState<TState>() where TState : State{
			State state;
			if(_stateCache.TryGetValue(typeof (TState), out state)){
				return (TState) state;
			}else{
				TState newState = Activator.CreateInstance<TState>();
				newState.Parent = this;
				newState.Init();
				_stateCache[typeof(TState)] = newState;
				return newState;
			}
		}

		public abstract class State{
			internal FSM<TContext> Parent {get; set;}

			protected TContext Context;
			//protected TContext Context => Parent._context;

			protected void TransitionTo<TState>() where TState : State{
				Parent.TransitionTo<TState>();
			}

			public virtual void Init(){
				Context = Parent._context;
			}

			public virtual void OnEnter(State previousState){ }

			public virtual void OnExit(State nextState){ }

			public virtual void Update(){ }

			public virtual void CleanUp(){ }
		}
	}
}
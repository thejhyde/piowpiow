﻿using UnityEngine;

namespace gc {

	public class Game : MonoBehaviour {

		public GameObject[] possibleEnemies;
		public GameObject background;

		void Awake(){
			Services.eventManager = new EventManager();
			Services.taskManager = new TaskManager();
			Services.enemyManager = new EnemyManager();
			Services.background = background.GetComponent<SpriteRenderer>();

			Services.sceneManager = new SceneManager<TransitionData>();
			Services.sceneManager.SceneRoot = GameObject.Find("SceneRoot");
			Services.sceneManager.PushScene<TitleScreen>();
			Services.possibleEnemies = possibleEnemies;
		}

		void Update () {
			Services.sceneManager.CurrentScene.Update();
		}

		void OnDestroy(){
			Services.enemyManager.Stop();
			Services.enemyManager = null;
			Services.eventManager = null;
			Services.taskManager = null;
		}
	}

}
using UnityEngine;

namespace gc {

	public class BackwardsGun : Gun {
        
		public override void Update(){
			base.Update();

            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 shipPosition = gameObject.transform.position;
            Vector2 offsetToMouse = mousePosition - shipPosition;

            if (offsetToMouse.magnitude > _minOffset) {
                Vector3 mouseDirection = offsetToMouse.normalized;
                _firingDirection = Vector3.zero - mouseDirection;
            }	

            fireGun(_firingDirection);
		}
	}
}
using UnityEngine;

namespace gc {

	public class BurstGun : Gun {
        private Vector3[] directions = new Vector3[4];


		public override void Update(){
			base.Update();

			// Aim the gun wherever the mouse is pointed
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 shipPosition = gameObject.transform.position;
            Vector2 offsetToMouse = mousePosition - shipPosition;

            // Only update the firing direction when the mouse isn't directly on top of the ship
            // (this is so microscopic changes in mouse position don't change the firing direction)
            if (offsetToMouse.magnitude > _minOffset) 
			{
                Vector3 mouseDirection = offsetToMouse.normalized;

                directions[0] = mouseDirection;
                directions[1] = Vector3.zero - mouseDirection;
                directions[2] =  Quaternion.Euler(0, 0, 90) * mouseDirection;
                directions[3] = Quaternion.Euler(0, 0, -90) * mouseDirection;
                //directions[0] = Vector3.up;
                //directions[1] = Vector3.left;
                //directions[2] = Vector3.down;
                //directions[3] = Vector3.right;
            }

            multiFire(directions);
		}
	}
}
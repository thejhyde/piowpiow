using UnityEngine;

namespace gc {

	public class ForwardGun : Gun {
		public override void Update(){
			base.Update();

			// Aim the gun wherever the mouse is pointed
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 shipPosition = gameObject.transform.position;
            Vector2 offsetToMouse = mousePosition - shipPosition;

            // Only update the firing direction when the mouse isn't directly on top of the ship
            // (this is so microscopic changes in mouse position don't change the firing direction)
            if (offsetToMouse.magnitude > _minOffset) 
			{
                Vector3 mouseDirection = offsetToMouse.normalized;
                _firingDirection = mouseDirection;
            }	

            fireGun(_firingDirection);
		}
	}
}
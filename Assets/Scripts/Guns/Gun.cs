using UnityEngine;

namespace gc {

	public abstract class Gun : MonoBehaviour {

        protected readonly static float _minOffset = 0.01f;

        protected Vector2 _firingDirection;
        public float FireInterval;
        protected float _nextFireTime;
        protected bool _isFiring = true;

        protected virtual void Start(){
            _firingDirection = Vector2.up;
        }

        public void StartFiring() {
            _isFiring = true;
        }

		public void StopFiring() {
            _isFiring = false;
        }

        public virtual void Update(){
            		
        }

        //Is it time to fire a bullet?
        protected bool fireTime(){
            return _isFiring && Time.time >= _nextFireTime;
        }

        // Fire one bullet
        protected void fireGun(Vector3 direction){
            if (fireTime()) {
                Fire(direction);
                _nextFireTime = Time.time + (FireInterval / 1000);
            }
        }

        //Fire multiple bullets
        protected void multiFire(Vector3[] directions){
            if (fireTime()) {
                for(int i = 0; i < directions.Length; i++){
                    Fire(directions[i]);
                }
                _nextFireTime = Time.time + (FireInterval / 1000);
            }
        }

        protected void Fire(Vector3 direction){
			Bullet.Fire(direction.normalized, gameObject.transform.position);
			AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/player_fire");
			AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position, 0.1f);
        }
    }
}

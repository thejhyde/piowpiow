using UnityEngine;

namespace gc {

	public class RandomGun : Gun {
		public override void Update(){
            fireGun(Random.insideUnitCircle);
		}
	}
}
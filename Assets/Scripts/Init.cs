﻿using UnityEngine;
using UnityEngine.Assertions;

namespace gc {

    public class Init : MonoBehaviour {
		
		private void Awake() {
			// If your environment supports assertions use them, and make sure that they halt execution as soon as the assertion fails
			Assert.raiseExceptions = true;

			SetupCamera();
			SetupCollisionFiltering();
        }
			
		private void SetupCamera() {
			var camera = Camera.main;
			float halfHeight = Config.ScreenHeight / 2;
			float halfWidth = Config.ScreenWidth / 2;
			camera.orthographicSize = halfHeight / Config.SpriteScale;
			camera.transform.position = new Vector3(halfWidth / Config.SpriteScale, halfHeight / Config.SpriteScale, -1);
		}

		private void SetupCollisionFiltering() {
			int playerLayer = GetLayerID("PLAYER");
			int bulletLayer = GetLayerID("BULLET");
			int enemyLayer = GetLayerID("ENEMY");
			int wallLayer = GetLayerID("WALL");

			// Bullets should not hit the player
			Physics2D.IgnoreLayerCollision(playerLayer, bulletLayer);

			// Bullets should not hit bullets
			Physics2D.IgnoreLayerCollision(bulletLayer, bulletLayer);

			// Enemies should not collide with one another
			Physics2D.IgnoreLayerCollision(enemyLayer, enemyLayer);

			// Enemies should not collide with walls
			Physics2D.IgnoreLayerCollision(enemyLayer, wallLayer);
		}

		private int GetLayerID(string layerName) {
			const int LAYER_ID_NOT_FOUND = -1;
			int ID = LayerMask.NameToLayer(layerName);
			Assert.AreNotEqual(LAYER_ID_NOT_FOUND, ID);
			return ID;
		}

    }

} // namespace gc

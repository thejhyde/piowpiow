﻿using UnityEngine;

namespace gc {

	public class Player : MonoBehaviour {

		public static Player Instance { get; private set; }
		public GameObject[] guns;

		public int Health = 100;
		public float MaxSpeed;

		protected Rigidbody2D body;

		void Awake() {
			//UnityEngine.Object resource = ResourceLoader.Instance.GetResource("Prefabs/ForwardGun");
            //GameObject gun = (GameObject)Instantiate(resource, transform.localPosition, Quaternion.identity);
            //gun.transform.parent = transform;

			Instance = this;

			body = GetComponent<Rigidbody2D>();
            if (body == null) {
                throw new MissingComponentException("MovementController added to game object that has no Rigidbody2D");
            }

		}

		void Update() {
			if (Health <= 0) {
				Services.eventManager.Fire(new PlayerDeathEvent(transform.localPosition.x, transform.localPosition.y));
				Destroy(gameObject);
			}

			// Clear the velocity every frame so the player's ship responds instantly to changes in direction
			body.velocity = Vector2.zero;
			body.angularVelocity = 0;

			// determine the positions of the mouse and player
			Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3 shipPosition = gameObject.transform.position;
            Vector3 offsetToMouse = mousePosition - shipPosition;

			// Don't bother updating the forces if the player is very close to the mouse
			float distanceToMouse = offsetToMouse.magnitude;
			if (distanceToMouse > Mathf.Epsilon) {

				// scale the speed so that the force gets smaller when the ship gets close to the mouse
				// NOTE: The scaling the distance by the inverse of the time step has to do with how Box2D handles forces vs. impulses.
				float speed = Mathf.Min(MaxSpeed, (distanceToMouse / Time.fixedDeltaTime));

				// calculate the direction of the force required to move the player towards the mouse
				Vector2 forceDirection = offsetToMouse.normalized;
				Vector2 force = forceDirection * speed;

				// apply the force as an impulse (impulses are easier if you're constantly clearing the forces like we do here)
				body.AddForce(force, ForceMode2D.Impulse);
			}
		}

		void ApplyDamage(int damage) {
			Health -= damage;
			if (Health <= 0) {
				AudioClip clip = (AudioClip)ResourceLoader.Instance.GetResource("Sounds/player_destroyed");
				AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
			}
		}

		void OnDestroy() {
			Instance = null;
		}

	}

}



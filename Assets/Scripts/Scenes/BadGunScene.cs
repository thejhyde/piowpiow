using UnityEngine;
using System.Collections.Generic;

namespace gc{
    public class BadGunScene : MainScene{
        internal override void OnEnter(Scene<TransitionData> previousScene){
            gunType = "ForwardGun";
            health = 999;
            timedWaves = true;
            getsShot = false;
            Services.background.color = Color.red;

            StartGame();
        }
    }
}
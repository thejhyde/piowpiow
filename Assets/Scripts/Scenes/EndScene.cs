using UnityEngine;
using System.Collections.Generic;

namespace gc{

    public class EndScene : Scene<TransitionData>{
    	
    	internal override void OnEnter(Scene<TransitionData> previousScene){
            Services.eventManager.UnregisterAll();
            Services.taskManager.AbortAllTasks();
            Services.enemyManager.Stop();

    		UnityEngine.Object resource = ResourceLoader.Instance.GetResource("Prefabs/End");
			GameObject objectToSpawn = Instantiate(resource, new Vector3(55, 50, 0), Quaternion.identity) as GameObject;
			objectToSpawn.transform.parent = GameObject.Find("EndScene").transform;

            WaitTask wait = new WaitTask(1000);
            Services.taskManager.AddTask(wait);
            wait.Then(new ActionTask(()=> { Services.sceneManager.MultiPop(3); }));
    	}

        internal override void Update(){
            Services.taskManager.Update();
        }
    }
}
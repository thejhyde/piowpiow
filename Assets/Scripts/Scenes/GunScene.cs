using UnityEngine;
using System.Collections.Generic;

namespace gc{
    public class GunScene : MainScene{
        internal override void OnEnter(Scene<TransitionData> previousScene){
            gunType = "ForwardGun";
            health = 100;
            timedWaves = false;
            getsShot = true;
            Services.background.color = Color.white;
            
            StartGame();
        }
    }
}
using UnityEngine;
using System.Collections.Generic;

namespace gc{

    public class MainScene : Scene<TransitionData>{

        protected string gunType;
        public bool timedWaves;
        public int health;
        public bool getsShot;

        protected void StartGame(){
            UnityEngine.Object resource = ResourceLoader.Instance.GetResource("Prefabs/Playership");
            GameObject player = (GameObject)Instantiate(resource, RandomWorldPosition(3), Quaternion.identity);

            UnityEngine.Object gunResource = ResourceLoader.Instance.GetResource("Prefabs/" + gunType);
            GameObject gun = (GameObject)Instantiate(gunResource, player.transform.localPosition, Quaternion.identity);
            gun.transform.parent = player.transform;

            Services.eventManager.Fire(new PlayerSpawnedEvent());
            Services.enemyManager.Start();

            Services.eventManager.Register<PlayerDeathEvent>(endScene);
        }

        internal override void Update(){
        	Services.enemyManager.Update();
            Services.taskManager.Update();
        }

        void endScene(Event e){
            Services.sceneManager.PushScene<EndScene>();
        }

        private Vector3 RandomWorldPosition(int gutter) {
			return new Vector3(Random.Range(gutter, Config.WorldWidth - gutter), Random.Range(gutter, Config.WorldHeight - gutter), 0); 
		}
    }
}
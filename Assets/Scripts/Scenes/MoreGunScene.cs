using UnityEngine;
using System.Collections.Generic;

namespace gc{
    public class MoreGunScene : MainScene{
        internal override void OnEnter(Scene<TransitionData> previousScene){
            gunType = "BurstGun";
            health = 200;
            timedWaves = false;
            getsShot = true;
            Services.background.color = Color.blue;
            StartGame();
        }
    }
}
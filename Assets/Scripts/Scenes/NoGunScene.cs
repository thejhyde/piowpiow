using UnityEngine;
using System.Collections.Generic;

namespace gc{
    public class NoGunScene : MainScene{
        internal override void OnEnter(Scene<TransitionData> previousScene){
            gunType = "NoGun";
            health = 999;
            timedWaves = true;
            getsShot = false;
            Services.background.color = Color.green;
            StartGame();
        }
    }
}
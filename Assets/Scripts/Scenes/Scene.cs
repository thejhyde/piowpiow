using System.Runtime.CompilerServices;
using UnityEngine;

namespace gc{
	public class Scene<TTransitionData> : MonoBehaviour{
		internal virtual TTransitionData GetTransitionData(){ return default(TTransitionData); }

		internal void _OnEnter(Scene<TTransitionData> previousScene){
			gameObject.SetActive(true);
			OnEnter(previousScene);
		}

		internal void _OnExit(Scene <TTransitionData> nextScene){
			gameObject.SetActive(false);
			OnExit(nextScene);
		}

		internal virtual void OnEnter(Scene<TTransitionData> previousScene){ }
		internal virtual void OnExit(Scene<TTransitionData> nextScene){ }
		internal virtual void Update(){ }
	}
}
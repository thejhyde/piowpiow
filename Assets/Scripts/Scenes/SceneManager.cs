using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace gc{
	public class SceneManager<TTransitionData>{
		internal GameObject SceneRoot{get; set;}

		private readonly Stack<Scene<TTransitionData>> _sceneStack = new Stack<Scene<TTransitionData>>();

		public Scene<TTransitionData> CurrentScene{
			get{
				return _sceneStack.Count != 0 ? _sceneStack.Peek() : null;
			}
		}

		public void PopScene(){
			Scene<TTransitionData> previousScene = null;
			Scene<TTransitionData> nextScene = null;

			if(_sceneStack.Count != 0){
				previousScene = _sceneStack.Peek();
				_sceneStack.Pop();
			}

			if(_sceneStack.Count != 0){
				nextScene = _sceneStack.Peek();
			}

			if(nextScene != null){
				nextScene._OnEnter(previousScene);
			}

			if(previousScene != null){
				previousScene._OnExit(nextScene);
				Object.Destroy(previousScene.gameObject);
			}
		}

		public void MultiPop(int pops){
			Scene<TTransitionData> previousScene = null;
			Scene<TTransitionData> nextScene = null;

			if(_sceneStack.Count != 0){
				previousScene = _sceneStack.Peek();
			}

			for(int i = 0; i < pops; i++){
				if(_sceneStack.Count != 0){
					_sceneStack.Pop();
				}else{
					i = pops;
				}
			}

			if(_sceneStack.Count != 0){
				nextScene = _sceneStack.Peek();
			}

			if(nextScene != null){
				nextScene._OnEnter(previousScene);
			}

			if(previousScene != null){
				previousScene._OnExit(nextScene);
				Object.Destroy(previousScene.gameObject);
			}
		}

		public void PushScene<T>() where T : Scene<TTransitionData>{
			var previousScene = CurrentScene;
			var nextScene = CreateScene<T>();

			_sceneStack.Push(nextScene);

			nextScene._OnEnter(previousScene);

			if(previousScene != null){
				previousScene._OnExit(nextScene);
				previousScene.gameObject.SetActive(false);
			}
		}

		public void Swap<T>() where T : Scene<TTransitionData>{
			Scene<TTransitionData> previousScene = null;
			if(_sceneStack.Count > 0){
				previousScene = _sceneStack.Peek();
				_sceneStack.Pop();
			}

			var nextScene = CreateScene<T>();
			_sceneStack.Push(nextScene);
			nextScene._OnEnter(previousScene);

			if(previousScene != null){
				previousScene._OnExit(nextScene);
				Object.Destroy(previousScene.gameObject);
			}
		}

		private T CreateScene<T>() where T : Scene<TTransitionData>{
			var sceneObject = new GameObject {name = typeof (T).Name};
			sceneObject.transform.SetParent(SceneRoot.transform);
			return sceneObject.AddComponent<T>();
		}
	}

	public struct TransitionData{
		//nothing for now
	}
}
using UnityEngine;
using System.Collections.Generic;

namespace gc{

    public class SelectScene : Scene<TransitionData>{
    	
    	internal override void OnEnter(Scene<TransitionData> previousScene){
    		UnityEngine.Object resource = ResourceLoader.Instance.GetResource("Prefabs/SelectionText");
			GameObject objectToSpawn = Instantiate(resource, new Vector3(55, 50, 0), Quaternion.identity) as GameObject;
			objectToSpawn.transform.parent = GameObject.Find("SelectScene").transform;
            //objectToSpawn.transform.localEulerAngles = new Vector3(0, 0, 170);
    	}

    	internal override void Update(){
    		if(Input.GetKeyDown(KeyCode.Alpha1)){
    			Services.sceneManager.PushScene<GunScene>();
    		}else if(Input.GetKeyDown(KeyCode.Alpha2)){
                Services.sceneManager.PushScene<MoreGunScene>();
            }else if(Input.GetKeyDown(KeyCode.Alpha3)){
                Services.sceneManager.PushScene<NoGunScene>();
            }else if(Input.GetKeyDown(KeyCode.Alpha4)){
                Services.sceneManager.PushScene<BadGunScene>();
            }

    	}
    }
}
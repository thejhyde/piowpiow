using UnityEngine;
using System.Collections.Generic;

namespace gc{

    public class TitleScreen : Scene<TransitionData>{
    	
    	internal override void OnEnter(Scene<TransitionData> previousScene){
    		//Spawner.Spawn("Prefabs/Title", new Vector3(55, 50, 0), 0, 6);
    		UnityEngine.Object resource = ResourceLoader.Instance.GetResource("Prefabs/Title");
			GameObject objectToSpawn = Instantiate(resource, new Vector3(55, 50, 0), Quaternion.identity) as GameObject;
			objectToSpawn.transform.parent = GameObject.Find("TitleScreen").transform;
    	}

    	internal override void Update(){
    		if(Input.GetKeyDown(KeyCode.Space)){
    			Services.sceneManager.PushScene<SelectScene>();
    		}
    	}
    }
}
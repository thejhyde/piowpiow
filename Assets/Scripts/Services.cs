using System.Diagnostics;
using UnityEngine;
using Debug = System.Diagnostics.Debug;

namespace gc {

	public abstract class Services {
		private static EnemyManager _enemy;
		public static EnemyManager enemyManager{
			get{
				Debug.Assert(_enemy != null);
				return _enemy;
			}
			set{ _enemy = value;}
		}

		private static EventManager _event;
		public static EventManager eventManager{
			get{
				Debug.Assert(_event != null);
				return _event;
			}
			set{ _event = value;}
		}

		private static TaskManager _task;
		public static TaskManager taskManager{
			get{
				Debug.Assert(_task != null);
				return _task;
			}
			set{ _task = value; }
		}

		private static SceneManager<TransitionData> _scene;
		public static SceneManager<TransitionData> sceneManager{
			get{
				Debug.Assert(_scene != null);
				return _scene;
			}
			set{ _scene = value;}
		}

		private static SpriteRenderer _background;
		public static SpriteRenderer background{
			get{
				Debug.Assert(_scene != null);
				return _background;
			}
			set{ _background = value;}
		}

		public static GameObject[] possibleEnemies;
	}
}
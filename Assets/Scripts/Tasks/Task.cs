using System;
using System.Diagnostics;

namespace gc {
	public abstract class Task{

		protected Task(){
			Status = TaskStatus.Detached;
		}

		public enum TaskStatus : byte {
			Detached,
			Pending,
			Working,
			Success,
			Fail,
			Aborted
		}

		public TaskStatus Status { get; private set; }

		//public bool IsAttached => Status != TaskStatus.Detached;
		public bool IsAttached(){
			return Status != TaskStatus.Detached;
		}

		public bool Finished(){
			return Status == TaskStatus.Success ||
			Status == TaskStatus.Fail ||
			Status == TaskStatus.Aborted;
		}

		public void Abort(){
			SetStatus(TaskStatus.Aborted);
		}

		internal void SetStatus(TaskStatus newStatus){
			if(Status == newStatus) return;

			Status = newStatus;

			switch(newStatus){
				case TaskStatus.Working:
					Init();
					break;

				case TaskStatus.Success:
					OnSuccess();
					CleanUp();
					break;

				case TaskStatus.Fail:
					OnFail();
					CleanUp();
					break;

				case TaskStatus.Aborted:
					OnAbort();
					CleanUp();
					break;

				case TaskStatus.Detached:
                case TaskStatus.Pending:
                    break;

                //default:
                    //throw new ArgumentOutOfRangeException(nameof(newStatus), newStatus, null);
			}
		}

		protected virtual void OnAbort(){}
		protected virtual void OnSuccess(){}
		protected virtual void OnFail(){}

		protected virtual void Init(){}
		internal virtual void Update(){}
		protected virtual void CleanUp(){}

		public Task NextTask{ get; private set; }

		public Task Then(Task task){
			Debug.Assert(!task.IsAttached());
			if(NextTask == null){
				NextTask = task;
			}else{
				NextTask.Then(task);
			}
			return task;
		}

		//public Task Then(Action action){
		//	var task = new ActionTask(action);
		//	return Then(task);
		//}
	}
}
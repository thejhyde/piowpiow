using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace gc{

	public class TaskManager{

		private readonly List<Task> _tasks = new List<Task>();

		public bool HasTasks(){
			return _tasks.Count > 0;
		}

		public void AddTask(Task task){
			Debug.Assert(task != null);
			Debug.Assert(!task.IsAttached());
			_tasks.Add(task);
			task.SetStatus(Task.TaskStatus.Pending);
		}

		public void AbortAllTasks(){
			for(int i = _tasks.Count - 1; i >= 0; --i){
				Task t = _tasks[i];
				t.Abort();
				_tasks.RemoveAt(i);
				t.SetStatus(Task.TaskStatus.Detached);
			}
		}

		public void Update(){
			for(int i = _tasks.Count - 1; i >= 0; --i){
				Task task = _tasks[i];
				if(task.Status == Task.TaskStatus.Pending){
					task.SetStatus(Task.TaskStatus.Working);
				}

				if(task.Finished()){
					HandleCompletion(task, i);
				}else{
					task.Update();
					if(task.Finished()){
						HandleCompletion(task, i);
					}
				}
			}
		}

		private void HandleCompletion(Task t, int taskIndex){
			if(t.NextTask != null && t.Status == Task.TaskStatus.Success){
				AddTask(t.NextTask);
			}
			_tasks.RemoveAt(taskIndex);
			t.SetStatus(Task.TaskStatus.Detached);
		}

		public void AbortAll<T>() where T : Task{
			Type type = typeof(T);
			for(int i = _tasks.Count - 1; i >= 0; i--){
				Task task = _tasks[i];
				if(task.GetType() == type){
					task.Abort();
				}
			}
		}
	}
}
using System;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace gc{

	public class WaitTask : Task{
		private static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1);
		private static double GetTimeStamp(){
			return (DateTime.UtcNow - UnixEpoch).TotalMilliseconds;
		}

		private readonly double _duration;
		private double _startTime;

		public WaitTask(double duration){
			this._duration = duration;
		}

		protected override void Init(){
			_startTime = GetTimeStamp();
		}

		internal override void Update(){
			double now = GetTimeStamp();
			bool durationElapsed = (now - _startTime) > _duration;
			if(durationElapsed){
				SetStatus(TaskStatus.Success);
			}
		}
	}

	public class ActionTask : Task{
		private readonly Action _action;

		public ActionTask(Action action){
			_action = action;
		}

		protected override void Init(){
			SetStatus(TaskStatus.Success);
			_action();
		}
	}

	public class WaitForTask : Task{
		private readonly Func<bool> _condition;

		public WaitForTask(Func<bool> condition){
			_condition = condition;
		}

		internal override void Update(){
			if(_condition()){
				SetStatus(TaskStatus.Success);
			}
		}
	}

	public class ScaleUpTask : Task{
		private readonly int _size;
		private Transform _t;

		public ScaleUpTask(Transform t, int size){
			_size = size;
			_t = t;
		}

		internal override void Update(){
			_t.localScale += new Vector3(Time.deltaTime, Time.deltaTime, 0);
			if(_t.localScale.x >= _size){
				SetStatus(TaskStatus.Success);
			}
		}
	}

	public class SpawnEnemyTask : Task{
		private readonly Vector3 _location;
		private readonly GameObject _g;
		private readonly int _health;
		private readonly bool _getsShot;

		public SpawnEnemyTask(Vector3 location, GameObject g, int h, bool getsShot){
			_location = location;
			_g = g;
			_health = h;
			_getsShot = getsShot;
		}

		internal override void Update(){
			GameObject enemy = GameObject.Instantiate(_g, _location, Quaternion.identity) as GameObject;
			enemy.GetComponent<Enemy>().MaxHealth = _health;
			enemy.GetComponent<Enemy>().SetShootReaction(_getsShot);
			Services.enemyManager.AddEnemy(enemy);
			SetStatus(TaskStatus.Success);
		}
	}
}